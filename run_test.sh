#!/bin/bash

set -e

bin="$1"

if ! [ -f "$bin" ]
then
  echo "usage: $0 EXECUTABLE"
  exit 1
fi

log_file="$(dirname "$bin")/log.txt"
touch "$log_file"

"$bin" 'localhost' 'does.not.matter' 'only.testing' 2>"$log_file" &
process_id=$!

sleep 5s

echo "$process_id"
kill "$process_id"
wait "$process_id"

echo "OUPUT:"
cat "$log_file"

if ! grep -F '[Perl callback]' "$log_file"
then
  echo "no output from perl callback"
  exit 1
fi
