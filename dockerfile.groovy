FROM ubuntu:groovy

ENV LANG=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
	&& apt-get -qy upgrade \
	&& apt-get -qy dist-upgrade \
	&& apt-get -qy install --no-install-recommends \
		build-essential \
		pkg-config \
		meson \
		libperl-dev \
		librdkafka-dev \
	&& apt-get -qy autoremove --purge \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

