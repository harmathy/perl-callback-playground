#!/bin/bash

set -e

HERE="$(dirname "$(realpath "$0")")"

build="$1"

if [ -z "$build" ]
then
  echo "usage: $0 BUILD_DIR"
  exit 1
fi

cd "$HERE"
meson "$build"

cd "$build"
ninja

ninja test || fail="yes"

if [ -z "$fail" ]
then
  # also print test log in positive case
  cat "$build/meson-logs/testlog.txt"
else
  exit 1
fi

