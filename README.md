# perl-callback-playground

This project showcases the use of a Perl function as callback to the [librdkafka](https://github.com/edenhill/librdkafka) logging functionality.

## Build

For building you need:

* Perl-Headers
* Meson (and Ninja)
* librdkafa (with headers)
* pkgconf

This project uses the Meson build system.

```
meson build
cd build
ninja
```

## Usage

```
./consumer /tmp/log.txt localhost a.client a.topic
```

## Test in container

There are several dockerfiles for testing different platforms. There is a build script, which builds the code and subsequently runs a test script.

Example usage of the Archlinux container: You can build the container with:

```
docker build -f dockerfile.arch -t perl-callback-playground-arch .
```

Then run build and test with:

```
docker run --rm -v $PWD:/src perl-callback-playground-container:arch /src/build.sh /build
```

The build script expects the source mounted under `/src`.

## License

This is based on the examples of librdkafka.

Copyright (c) 2012-2020, Magnus Edenhill  
Copyright (c) 2020, Max Harmathy 

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
